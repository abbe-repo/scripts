-- Library to parse .torrent files
-- Author: Ashish SHUKLA (gmail.com!wahjava)
-- License: FreeBSD License
-- Copyright (C) 2009. Ashish SHUKLA

module Torrent
where

import Data.Int
import System.IO
import Data.Char
import qualified Data.ByteString.Char8 as B
import Data.Word

data BValue = BDictionary [(BValue, BValue)]
            | BList [BValue]
            | BNumber Int64
            | BString B.ByteString
              deriving Show

data Torrent = Torrent BValue
               deriving Show

torrentFile = "test.torrent"

emptyBS = B.empty

parseBDictionary :: B.ByteString -> (Maybe BValue, B.ByteString)
parseBDictionary string = let (d, s) = parseBDictionary' [] string
                          in (Just $ BDictionary d, s)
                              where parseBDictionary' xs string = if (B.length string) == 0
                                                                  then (xs, emptyBS)
                                                                  else if ((B.head $ string) ==  'e')
                                                                       then (xs, B.tail string)
                                                                       else let (k,r1) = parseBValue string
                                                                                (v,r2) = parseBValue r1
                                                                            in parseBDictionary' (xs ++ [(d k, d v)]) r2
                                    d (Just x) = x

parseBList :: B.ByteString -> (Maybe BValue, B.ByteString)
parseBList string = let (d, s) = parseBList' [] string
                    in (Just $ BList d, s)
                        where parseBList' xs string = if ((B.head $ string) ==  'e')
                                                      then (xs, B.tail string)
                                                      else let (v, r1) = parseBValue string
                                                           in parseBList' (xs ++ [d v]) r1
                              d (Just x) = x

parseBValue :: B.ByteString -> (Maybe BValue, B.ByteString)
parseBValue string | (B.head $ string) == 'd' = parseBDictionary (B.tail string)
                   | isDigit (B.head $ string) = let (length, rest) = B.break (==  ':') string
                                                     (rest1, rest2) = B.splitAt (read $ B.unpack $ length :: Int) (B.tail rest)
                                               in (Just $ BString rest1, rest2)
                   | (B.head $ string) == 'i' = let (number, rest) = B.break (== 'e') (B.tail string)
                                                in (Just $ BNumber (read $ B.unpack $ number :: Int64), B.tail rest)
                   | (B.head $ string) == 'l' = parseBList (B.tail string)

parseTorrent :: B.ByteString -> IO (Maybe Torrent)
parseTorrent torrentData = do case (parseBValue torrentData) of
                                (Nothing, _) -> return Nothing
                                (Just value, x) -> do return $ Just (Torrent value)

readTorrent handle = B.hGetContents handle >>= parseTorrent

main = do torrent <- withBinaryFile torrentFile ReadMode readTorrent
          putStrLn $ show $ torrent
