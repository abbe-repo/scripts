import System.Directory (getDirectoryContents, removeFile)
import Control.Monad (filterM, mapM_)
import List (isPrefixOf, find)
import System.Posix.Files (getFileStatus, isRegularFile)
import System.Locale (defaultTimeLocale)
import Data.Time.Clock (getCurrentTime, utctDay)
import Data.Time.Format (parseTime)
import Char (isSpace)

import System.IO
import Data.Time.Calendar

today :: IO Integer
today = getCurrentTime >>= return . toModifiedJulianDay . utctDay

dayCount :: Integer
dayCount = 60

defaultParseString :: String
defaultParseString = "%a, %e %b %Y %T %Z"
              

isExpirable :: Integer -> FilePath -> IO Bool
isExpirable t path = do withFile path ReadMode (\h -> hGetContents h >>= (\s -> return $! isExpirableMail t s))

isExpirableMail :: Integer -> String -> Bool
isExpirableMail t s = case find ("Date:" `isPrefixOf`) (lines s) of
                        Nothing -> False
                        Just x ->  oldEnough x t
    where oldEnough :: String -> Integer -> Bool
          oldEnough s t = let d  = dateString s
                              d1 = parseTime defaultTimeLocale defaultParseString d :: Maybe Day
                          in  case d1 of
                                Nothing -> False
                                Just d  -> if (t - (toModifiedJulianDay d)) > dayCount
                                           then True
                                           else False

dateString = dropWhile isSpace . tail . dropWhile (/= ':')
                      

main :: IO ()
main = do t <- today
          (getDirectoryContents ".") >>= (filterM isRF) >>= (filterM (isExpirable t)) >>= (mapM_  removeFile)
    where isRF fp = getFileStatus fp >>= (return . isRegularFile)
