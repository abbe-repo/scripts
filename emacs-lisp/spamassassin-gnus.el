;;; spamassassin-gnus.el - Integration of SpamAssassin in gnus

;; Copyright (C) 2008 Ashish Shukla.

;; Author: Ashish Shukla <wahjava@gmail.com>
;; Maintainer: Ashish Shukla <wahjava@gmail.com>
;; Created: 19 Jul 2008
;; Version: 0.01
;; gnus, spamassassin

;; This code allows you to classify mails as spam/ham
;; in SpamAssassin's database and respool it.
;; Tested with Gnus shipping with Emacs CVS 23.0.60.1

(defun my-gnus-mark-spam()
  (interactive)
  (gnus-summary-show-raw-article)
  (gnus-summary-save-in-pipe "spamc -L spam")
  (gnus-summary-show-article)
  ;; substitute "nnmaildir:spam" with the name of group holding "spam"
  (gnus-summary-move-article nil "nnmaildir:spam"))

(defun my-gnus-mark-ham()
  (interactive)
  (gnus-summary-show-raw-article)
  (gnus-summary-save-in-pipe "spamc -L ham")
  (gnus-summary-show-article)
  (gnus-summary-respool-article nil))

;; in summary mode use "B s" and "B h" keys to mark a mail as spam and ham
;; respectively for SpamAssassin to learn, and to also move to "spam" maildir
;; or respool mail accordingly
(define-key gnus-summary-mode-map (kbd "B s") 'my-gnus-mark-spam)
(define-key gnus-summary-mode-map (kbd "B h") 'my-gnus-mark-ham)
