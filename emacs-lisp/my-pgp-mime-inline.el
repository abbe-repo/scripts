;;; my-pgp-mime-inline.el --- PGP/MIME and PGP/Inline mails in Gnus

;; Copyright (C) 2008 Ashish Shukla.

;; Author: Ashish Shukla <wahjava@gmail.com>
;; Maintainer: Ashish Shukla <wahjava@gmail.com>
;; Created: 19 Jul 2008
;; Version: 0.01
;; gnus, pgp, mime, inline

;; This code lets you send PGP signed mail in the way recipient accepts.
;; Tested with Gnus shipping with Emacs CVS 23.0.60.1

(defcustom my-inline-pgp-mails-list
  '()
  "List of email address which only accept inline-PGP signed mails"
  :type '(repeat string))

(defun my-list-all-recipients()
  "Lists all recipients in the current buffer"
  (interactive)
  (let ((list-of-recipients)
        (list-of-emails '()))
    (setq list-of-recipients (split-string (concat (message-fetch-field "to") ","
                                     (message-fetch-field "bcc") ","
                                     (message-fetch-field "cc")) ","))
    (dolist (recipient list-of-recipients)
      (when (string-match "\\([[:alnum:].-]+@[[:alnum:].-]*\\)" recipient)
        (add-to-list 'list-of-emails (match-string 1 recipient))))
    list-of-emails))

(defun my-sign-mail()
  "Sends a PGP signed mail depending on whether recipient allows PGP/MIME signed mails"
  (let ((recipients (my-list-all-recipients))
        (message-signed nil))
    (dolist (email my-inline-pgp-mails-list)
      (when (member email recipients)
        (mml-secure-sign-pgp)
        (setq message-signed t)
        (return t)))
    (unless message-signed
      (mml-secure-sign-pgpmime))))


;;; mailing-lists which only accept pgp-inline mails
(add-to-list 'my-inline-pgp-mails-list "list-1@domain.tld")
(add-to-list 'my-inline-pgp-mails-list "list-2@domain.tld")

;; auto-sign all outgoing messages
(add-hook 'message-send-hook 'my-sign-mail)

(provide 'my-pgp-mime-inline)
;;; pgp-mime-inline.el ends here
